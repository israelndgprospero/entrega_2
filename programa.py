import hashlib

class Usuario:
    def __init__(self, nome, senha, tipo):
        self.nome = nome
        self.senha_hash = self._hash_senha(senha)
        self.tipo = tipo

    def _hash_senha(self, senha):
        return hashlib.sha256(senha.encode()).hexdigest()

    def verificar_senha(self, senha):
        return self.senha_hash == self._hash_senha(senha)

def criar_usuarios():
    gabriel = Usuario("Gabriel", "admin123", "administrador")
    lucas = Usuario("Lucas", "mod456", "moderador")
    marcio = Usuario("Marcio", "senha123", "visitante")
    joao_pedro = Usuario("JoaoPedro", "123", "visitante")
    return [gabriel, lucas, marcio, joao_pedro]

def main():
    usuarios = criar_usuarios()

    while True:
        nome = input("Nome de usuário (digite SAIR para interromper): ")

        # Verifica se o nome de usuário é "SAIR" e, se for, encerra o programa
        if nome == "SAIR":
            break

        senha = input("Senha: ")

        # O nome do usuário deve possuir apenas letras (não são permitidos espaços e caracteres especiais)
        if not nome.isalpha():
            print("Nome de usuário inválido. Deve conter apenas letras.")
            continue

        # Procura o usuário pelo nome
        usuario_encontrado = None
        for usuario in usuarios:
            if usuario.nome == nome:
                usuario_encontrado = usuario
                break

        if usuario_encontrado is None:
            print("Nome de usuário não encontrado.")
            continue

        # Verifica o hash da senha
        if usuario_encontrado.verificar_senha(senha):
            print(f"Bem-vindo, {nome}! Você é um {usuario_encontrado.tipo}.")
        else:
            print("Senha incorreta.")

if __name__ == "__main__":
    main()
